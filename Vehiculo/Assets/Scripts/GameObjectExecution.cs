﻿using UnityEngine;
using System.Collections;

public class GameObjectExecution : MonoBehaviour {
	public GameObject centro;
	public GameObject cola;
	public GameObject cuerpoArriba;
	public GameObject farolDerecha;
	public GameObject farolIzquierda;
	public GameObject farolAtrasDerecha;
	public GameObject farolAtrasIzquierda;
	public GameObject frente;
	public GameObject parrila;
	public GameObject placaFrente;
	public GameObject placaAtras;
	public GameObject plano;
	public GameObject rueda1;
	public GameObject rueda2;
	public GameObject rueda3;
	public GameObject rueda4;
	public GameObject ventanaIzquierda;
	public GameObject ventadaDerecha;
	public GameObject ventanaPrincipal;
	public GameObject ventanaTracera;

	/*public float smooth = 2.0f;
	public float Angle = 30.0f;*/


	void Creacion() {
		//Luz
		GameObject luz = new GameObject ("Luz");
		luz.AddComponent<Light> ();
		luz.light.type = LightType.Directional;
		luz.light.color = Color.white;
		luz.transform.position = new Vector3 (0, 15, -15);
		luz.transform.rotation = Quaternion.Euler(45, 0, 0);
		//Centro
		centro = GameObject.CreatePrimitive(PrimitiveType.Cube);
		centro.name = "Centro";
		centro.transform.position = new Vector3(0,0.3f,0);
		centro.transform.localScale = new Vector3 (5, 0.6f, 3);
		Material textura = Resources.Load("GrisMate", typeof(Material)) as Material;
		centro.renderer.material = textura;
		//cola
		cola = GameObject.CreatePrimitive (PrimitiveType.Plane);
		cola.name = "Cola";
		cola.transform.position = new Vector3 (2.76f, 0.3f, 0);
		cola.transform.rotation = Quaternion.Euler(50, 90, 0);
		cola.transform.localScale = new Vector3 (0.3f, 0, 0.08f);
		//cuerpoArriba
		cuerpoArriba = GameObject.CreatePrimitive (PrimitiveType.Cube);
		cuerpoArriba.name="cuerpoArriba";
		cuerpoArriba.transform.position = new Vector3 (0.2f, 0.85f, 0);
		cuerpoArriba.transform.localScale = new Vector3 (2.6f, 0.5f, 2.7f);
		//farolDerecha
		farolDerecha = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		farolDerecha.name = "farolDerecha";
		farolDerecha.transform.position = new Vector3 (-2.8f, 0.28f, 1);
		farolDerecha.transform.rotation = Quaternion.Euler (0, 0, 320);
		farolDerecha.transform.localScale = new Vector3 (0, 0.3f, 0.3f);
		//farolIzquierda
		farolIzquierda = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		farolIzquierda.name = "farolIzquierda";
		farolIzquierda.transform.position = new Vector3 (-2.8f, 0.28f, -1);
		farolIzquierda.transform.rotation = Quaternion.Euler (0, 0, 320);
		farolIzquierda.transform.localScale = new Vector3 (0, 0.3f, 0.3f);
		//farolAtraDerecha
		farolAtrasDerecha = GameObject.CreatePrimitive (PrimitiveType.Plane);
		farolAtrasDerecha.name = "farolAtrasDerecha";
		farolAtrasDerecha.transform.position = new Vector3 (2.8f, 0.27f, 1.1f);
		farolAtrasDerecha.transform.rotation = Quaternion.Euler (0, 0, 310);
		farolAtrasDerecha.transform.localScale = new Vector3 (0.03f , 100 , 0.06f);
		//farolAtrasIzquierda
		farolAtrasIzquierda = GameObject.CreatePrimitive (PrimitiveType.Plane);
		farolAtrasIzquierda.name = "farolAtrasIzquierda";
		farolAtrasIzquierda.transform.position = new Vector3 (2.8f, 0.27f, -1.1f);
		farolAtrasIzquierda.transform.rotation = Quaternion.Euler (0, 0, 310);
		farolAtrasIzquierda.transform.localScale = new Vector3 (0.03f, 100 , 0.06f);
		//frente
		frente = GameObject.CreatePrimitive (PrimitiveType.Plane);
		frente.name = "frente";
		frente.transform.position = new Vector3 (-2.78f, 0.3f, 0);
		frente.transform.rotation = Quaternion.Euler (310, 90, 0);
		frente.transform.localScale = new Vector3 (0.3f, 0 , 0.08f);
		//parrilla
		parrila = GameObject.CreatePrimitive (PrimitiveType.Plane);
		parrila.name = "parrila";
		parrila.transform.position = new Vector3 (-2.9f, 0.2f, 0);
		parrila.transform.rotation = Quaternion.Euler (310, 90, 0);
		parrila.transform.localScale = new Vector3 (0.15f, 0 , 0.04f);
		//placaFrente
		placaFrente = GameObject.CreatePrimitive (PrimitiveType.Plane);
		placaFrente.name = "placaFrente";
		placaFrente.transform.position = new Vector3 (-3, 0.12f, 0);
		placaFrente.transform.rotation = Quaternion.Euler (310, 90, 0);
		placaFrente.transform.localScale = new Vector3 (0.04f, 100 , 0.02f);
		//placaAtras
		placaAtras = GameObject.CreatePrimitive (PrimitiveType.Plane);
		placaAtras.name = "placaAtras";
		placaAtras.transform.position = new Vector3 (3, 0.12f, 0);
		placaAtras.transform.rotation = Quaternion.Euler (50, 90, 0);
		placaAtras.transform.localScale = new Vector3 (0.04f, 100 , 0.02f);
		//plano
		plano = GameObject.CreatePrimitive (PrimitiveType.Plane);
		plano.name = "plano";
		plano.transform.localScale = new Vector3 (0.6f, 1, 0.3f);
		//rueda1
		rueda1 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda1.name = "rueda1";
		rueda1.transform.position = new Vector3 (1.6f, 0, 1.2f);
		rueda1.transform.localScale = new Vector3 (1, 1 , 0.3f);
		//rueda2
		rueda2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda2.name = "rueda2";
		rueda2.transform.position = new Vector3 (1.6f, 0, -1.2f);
		rueda2.transform.localScale = new Vector3 (1, 1 , 0.3f);
		//rueda3
		rueda3 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda3.name = "rueda3";
		rueda3.transform.position = new Vector3 (-1.6f, 0, 1.2f);
		rueda3.transform.localScale = new Vector3 (1, 1 , 0.3f);
		//rueda4
		rueda4 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		rueda4.name = "rueda4";
		rueda4.transform.position = new Vector3 (-1.6f, 0, -1.2f);
		rueda4.transform.localScale = new Vector3 (1, 1 , 0.3f);
		//ventanaDerecha
		ventadaDerecha = GameObject.CreatePrimitive (PrimitiveType.Plane);
		ventadaDerecha.name = "ventanaDerecha";
		ventadaDerecha.transform.position = new Vector3 (0.2f, 0.85f, 1.36f);
		ventadaDerecha.transform.rotation = Quaternion.Euler (90, 0, 0);
		ventadaDerecha.transform.localScale = new Vector3 (0.26f, 0, 0.05f);
		//ventanaIzquierda
		ventanaIzquierda = GameObject.CreatePrimitive (PrimitiveType.Plane);
		ventanaIzquierda.name = "ventanaIzquierda";
		ventanaIzquierda.transform.position = new Vector3 (0.2f, 0.85f, -1.36f);
		ventanaIzquierda.transform.rotation = Quaternion.Euler (270, 0, 0);
		ventanaIzquierda.transform.localScale = new Vector3 (0.26f, 0, 0.05f);
		//ventanaPrincipal
		ventanaPrincipal = GameObject.CreatePrimitive (PrimitiveType.Cube);
		ventanaPrincipal.name = "ventanaPrincipal";
		ventanaPrincipal.transform.position = new Vector3 (-1.27f, 0.55f, 0);
		ventanaPrincipal.transform.rotation = Quaternion.Euler (0, 0, 45);
		ventanaPrincipal.transform.localScale = new Vector3 (1, 0.5f, 2.7f);
		//ventanaTracera
		ventanaTracera = GameObject.CreatePrimitive (PrimitiveType.Cube);
		ventanaTracera.name = "ventanaTracera";
		ventanaTracera.transform.position = new Vector3 (1.68f, 0.55f, 0);
		ventanaTracera.transform.rotation = Quaternion.Euler (0, 0, 135);
		ventanaTracera.transform.localScale = new Vector3 (1, 0.5f, 2.7f);

	}
	void Start(){
		Creacion ();
	}
}
